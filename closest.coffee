fs = require 'fs'
_ = require 'underscore'

arg = process.argv[2]

Config =
  closest_count: 4
  closest_min_tolerance: 0.4
  closest_max_tolerance: 1.5
  closest_valid: 2

class Point
  constructor: (@x, @y) ->
  
  distance: (another_point) ->
    Math.sqrt(Math.pow(@x - another_point.x, 2), Math.pow(@y - another_point.y, 2))

parsed = JSON.parse(fs.readFileSync("tmp/#{arg}_pairs.json", 'utf-8'))
pairs = parsed.pairs

passed = []

for pair in pairs[0..]
  closests = []
  largest_distance = 1000000
  choosen = new Point(pair.a.x, pair.a.y)
  
  for another_pair in pairs
    another_point = new Point(another_pair.a.x, another_pair.a.y)
    distance = choosen.distance another_point
    
    if 0.00001 < distance < largest_distance
      if closests.length < Config.closest_count
        closests.push {pair: another_pair, distance: distance}
        largest_distance = _.max(closests, (el) -> el.distance).distance
      else
        for closest_no in [0...closests.length]
          if closests[closest_no] == largest_distance
            closests[closest_no] = {pair: another_pair, distance}
            largest_distance = _.max(closests, (el) -> el.distance).distance
            break
  
  inRange = 0
  
  #console.error closests
  console.error largest_distance
  
  if largest_distance < 0.15
    for closest in closests
      closest_target_point = new Point(closest.pair.b.x, closest.pair.b.y)
      target_point = new Point(pair.b.x, pair.b.y)
      if largest_distance * Config.closest_min_tolerance <= closest_target_point.distance(target_point) <= largest_distance * Config.closest_max_tolerance
        inRange += 1
      
  if inRange >= (Config.closest_valid)
    passed.push _.extend pair, {closest_a: closests[0], closest_b: closests[1]}
  
console.error "Pairs #{pairs.length}, passed #{passed.length}"
  
console.log JSON.stringify {dimensions: parsed.dimensions, pairs: passed}
