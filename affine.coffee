fs = require 'fs'
sm = require 'simplematrix'

Utils =
  random: (min, max) ->
    max += 1
    Math.floor(Math.random() * (max - min) + min)
    
  roll: (possibility) ->
    Math.random() < possibility

class Point
  constructor: (@x, @y) ->
  
class Pair
  constructor: (@point_a, @point_b) ->

arg = process.argv[2]

parsed = JSON.parse(fs.readFileSync("tmp/#{arg}_pairs.json", 'utf-8'))
pairs = parsed.pairs
dimensions = parsed.dimensions
  
Config =
  width: dimensions.a.width
  height: dimensions.a.height
  ransac:
    roll_count: 1000
    min_distance: dimensions.a.height * 0.01
    max_distance: dimensions.a.height * 0.50
    tolerance: dimensions.a.height * 0.09

affines = []
printed = 0

console.error new Date()

i = 0
while affines.length < pairs.length
  #if i > pairs.length * 10000
    #console.error 'broken'
    #break
  i += 1
  
  progress = Math.round(affines.length * 100 / pairs.length)
  if progress > printed and progress % 5 == 0
    process.stderr.clearLine()
    process.stderr.cursorTo(0)
    process.stderr.write "Rolling triples... #{progress}%"
    printed = progress

  selected_no = Utils.random(0, pairs.length-1)
  selected = [pairs[selected_no]]
  selected_nos = [selected_no]
  
  while selected.length < 3
    candidate_no = Utils.random(0, pairs.length-1)
    candidate = pairs[candidate_no]
    
    unless candidate_no in selected_nos
      selected.push candidate
      selected_nos.push candidate_no

  triple = selected
  
  a = triple[0].a
  b = triple[1].a
  c = triple[2].a

  u = triple[0].b
  v = triple[1].b
  w = triple[2].b

  continue  unless Math.pow(Config.ransac.min_distance, 2) < Math.pow((a.x - b.x) * Config.width, 2) + Math.pow((a.y - b.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2) and Math.pow(Config.ransac.min_distance, 2) < Math.pow((u.x - v.x) * Config.width, 2) + Math.pow((u.y - v.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2)
  continue  unless Math.pow(Config.ransac.min_distance, 2) < Math.pow((a.x - c.x) * Config.width, 2) + Math.pow((a.y - c.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2) and Math.pow(Config.ransac.min_distance, 2) < Math.pow((u.x - w.x) * Config.width, 2) + Math.pow((u.y - w.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2)
  continue  unless Math.pow(Config.ransac.min_distance, 2) < Math.pow((b.x - c.x) * Config.width, 2) + Math.pow((b.y - c.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2) and Math.pow(Config.ransac.min_distance, 2) < Math.pow((v.x - w.x) * Config.width, 2) + Math.pow((v.y - w.y) * Config.height, 2) < Math.pow(Config.ransac.max_distance, 2)

  A = new sm.Matrix([
    [a.x, a.y, 1, 0, 0, 0]
    [b.x, b.y, 1, 0, 0, 0]
    [c.x, c.y, 1, 0, 0, 0]
    [0, 0, 0, a.x, a.y, 1]
    [0, 0, 0, b.x, b.y, 1]
    [0, 0, 0, c.x, c.y, 1]
  ]);

  B = new sm.Matrix([[u.x], [v.x], [w.x], [u.y], [v.y], [w.y]])

  #console.error A

  params = A.inverse().times(B)

  affine = new sm.Matrix([
    [params[0], params[1], params[2]]
    [params[3], params[4], params[5]]
    [0, 0, 1]
  ])
  
  affines.push affine

console.error ""

best_affine = null
best_found = 0

i = 0
printed = 0

for affine in affines[0..]
  progress = Math.round(i * 100 / affines.length)
  i += 1
  
  if progress > printed and progress % 5 == 0
    process.stderr.clearLine()
    process.stderr.cursorTo(0)
    process.stderr.write "Checking affine matrices... #{progress}%"
    printed = progress
    
  found = 0
  for pair in pairs[0..]
    projected = affine.times(new sm.Matrix([[pair.a.x], [pair.a.y], [1]]))
    distance = Math.sqrt(Math.pow(projected[0] - pair.b.x, 2) + Math.pow(projected[1] - pair.b.y, 2))
    found += 1  if distance * Config.height < (Config.height * 0.01)
  
  if found > best_found
    best_affine = affine
    best_found = found
    
console.error ""
console.error best_affine, best_found

correct_pairs = []

for pair in pairs[0..]
  projected = best_affine.times(new sm.Matrix([[pair.a.x], [pair.a.y], [1]]))
  distance = Math.sqrt(Math.pow(projected[0] - pair.b.x, 2) + Math.pow(projected[1] - pair.b.y, 2))
  correct_pairs.push pair  if distance * Config.height < Config.ransac.tolerance
  
console.error new Date()

console.log JSON.stringify {dimensions: dimensions, pairs: correct_pairs}
