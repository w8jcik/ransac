Utils =
  random: (min, max) ->
    max += 1
    Math.floor(Math.random() * (max - min) + min)
    
  roll: (possibility) ->
    Math.random() < possibility

Config =
  dimensions:
    a:
      width: null
      height: null
    b:
      width: null
      height: null
  pair_name: undefined
    
(->
  x_margin = 10
  y_margin = 10
  
  x_offset = 280
  y_offset = 180

  canvas = @__canvas = new fabric.Canvas("c")
  fabric.Object::transparentCorners = false

  draw = (pair_name) ->
    Config.pair_name = pair_name
    canvas.clear().renderAll()

    drawLine = (a_x, a_y, b_x, b_y, color, strokeWidth) ->
      left = x_margin + a_x * Config.dimensions.a.width
      top = y_margin + a_y * Config.dimensions.a.height
      calc_end_x = (b_x * Config.dimensions.b.width) + x_offset + x_margin
      calc_end_y = (b_y * Config.dimensions.b.height) + y_offset + y_margin
    
      line = new fabric.Line([
        0
        0
        calc_end_x - left
        calc_end_y - top
      ],
        left: left
        top: top
        stroke: color or= "rgb(#{Utils.random(0, 255)}, #{Utils.random(0, 255)}, #{Utils.random(0, 255)})"
        strokeWidth: strokeWidth or= 1
        hasControls: false
        selectable: false
        hasBorders: false
      )
      canvas.add line
      return

    fabric.Image.fromURL "tmp/#{Config.pair_name}a.png", (img) ->
      img.set
        left: x_margin
        top: y_margin
        hasControls: false
        selectable: false
        hasBorders: false
        
      canvas.insertAt(img, 1)

      if false
        $.get "./tmp/#{Config.pair_name}_pairs.json", (data) ->
          parsed = JSON.parse(data)
          pairs = parsed.pairs
          console.log "Pair count...", pairs.length
          dimensions = parsed.dimensions
          Config.dimensions = dimensions
          
          #drawLine(0, 0, 0, 0, "rgb(0, 255, 0)")
          #drawLine(1, 1, 1, 1, "rgb(255, 0, 0)")
          
          i = 0
          for pair in pairs[0..]
            #if true or i % 20 == 0
            drawLine(pair.a.x, pair.a.y, pair.b.x, pair.b.y, "rgb(255, 0, 0)", 2)  if pair
            i += 1
      
      $.get "./tmp/#{Config.pair_name}_closest.json", (data) ->
        parsed = JSON.parse(data)
        pairs = parsed.pairs
        console.log "Pair count...", pairs.length
        dimensions = parsed.dimensions
        Config.dimensions = dimensions
        
        i = 0
        for pair in pairs[0..]
          #if true or i % 20 == 0
          r = Utils.random(0, 235)
          g = Utils.random(0, 235)
          b = Utils.random(0, 235)
          color_a = "rgb(#{r}, #{g}, #{b})"
          color_b = "rgb(#{r+20}, #{g+20}, #{b+20})"
          drawLine(pair.a.x, pair.a.y, pair.b.x, pair.b.y, "rgb(0,255,0)")  if pair
          #drawLine(pair.closest_a.pair.a.x, pair.closest_a.pair.a.y, pair.closest_a.pair.b.x, pair.closest_a.pair.b.y, color_b, 2)  if pair
          #drawLine(pair.closest_b.pair.a.x, pair.closest_b.pair.a.y, pair.closest_b.pair.b.x, pair.closest_b.pair.b.y, color_b, 2)  if pair
          i += 1

    fabric.Image.fromURL "tmp/#{Config.pair_name}b.png", (img) ->
      img.set
        left: x_margin + x_offset
        top: y_margin + y_offset
        hasControls: false
        selectable: false
        hasBorders: false
        
      canvas.insertAt(img, 0)
  
  draw("c")
    
)()
