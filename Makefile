client:
	coffee -wc client.coffee

from_scratch:
	-make clean
	make all_images
	make all_points
	make reprocess

reprocess:
	make all_pairs
	make all_affine
	make client

affine:
	coffee affine.coffee ${PAIR} > tmp/${PAIR}_affine.json

closest:
	coffee closest.coffee ${PAIR} > tmp/${PAIR}_closest.json

pairs:
	coffee pairs.coffee ${PAIR} > tmp/${PAIR}_pairs.json

all_affine:
	for pair in a b c d e f g h; do \
		make PAIR=$$pair affine; done

all_closest: 
	for pair in a b c d e f g h; do \
		make PAIR=$$pair closest; done

all_pairs: 
	for pair in a b c d e f g h; do \
		make PAIR=$$pair pairs; done

all_images:
	mogrify -path ./tmp -resize 320x320 -format png ./data/*.jpeg

all_points:
	for image in ./tmp/*.png ; do \
		bin/extract -haraff -sift -i $$image -DE; done
	rm tmp/*.params

summary:
	coffee summary.coffee

clean:
	rm tmp/*

