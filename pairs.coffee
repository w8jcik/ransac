fs = require 'fs'
moment = require 'moment'
size = require('image-size')

class Point
  constructor: (@id, @x, @y, @features) ->

  sameFeatures: (another_point) ->
    return false  if @features.length != another_point.features.length
    
    same = true
    for i in [0..@features.length]
      if @features[i] != another_point.features[i]
        same = false
    same

  getSimilarity: (another_point) ->    
    similarity = 0
    for i in [0...@features.length]
      similarity += Math.abs(@features[i] - another_point.features[i])
    similarity

class Image
  constructor: (@dimensions) ->
    @points = []
    
  addPoint: (point) ->
    @points[point.id] = point
    
  parseHaraff: (filename) ->
    raw = new String(fs.readFileSync filename)
    id = 0
    
    for line in raw.split("\n")[2..]
      chopped = line.split(" ")
      x = (parseFloat chopped[0]) / @dimensions.width
      y = (parseFloat chopped[1]) / @dimensions.height
      features = (parseInt(feature) for feature in chopped[5..])
      if x > 1 or y > 1
        console.error "Point outside", x, y  
      else
        @addPoint new Point(id, x, y, features)
      id += 1
      
  getRefs: (another_image) ->  
    found = []
    printed = 0
    
    for point in @points
    
      progress = Math.round(point.id * 100 / @points.length)
      if progress > printed and progress % 5 == 0
        process.stderr.clearLine()
        process.stderr.cursorTo(0)
        process.stderr.write "Searching for references... #{progress}%"
        printed = progress

      bestMatch = 256 * 128
      bestPoint = undefined
      
      for another_image_point in another_image.points
        match = point.getSimilarity another_image_point    
        if bestMatch > match
          bestPoint = another_image_point
          bestMatch = match
          
      found[point.id] = bestPoint  # if bestMatch < (256 * 128) / 15

    console.error ""
    return found

arg = process.argv[2]

start_stamp = moment()
console.error "[#{start_stamp.format("HH:mm:ss")}] Pair '#{arg}' Start..."

dimensions = size "tmp/#{arg}a.png"

a = new Image {width: dimensions.width, height: dimensions.height}
a.parseHaraff "tmp/#{arg}a.png.haraff.sift"

dimensions = size "tmp/#{arg}b.png"

b = new Image {width: dimensions.width, height: dimensions.height}
b.parseHaraff "tmp/#{arg}b.png.haraff.sift"

refs = a.getRefs b
rev_refs = b.getRefs a

console.error "References count... #{refs.length} #{rev_refs.length}"

#console.log ((ref.id if ref) for ref in refs)
#console.log ((ref.id if ref) for ref in rev_refs)

stop_stamp = moment()
duration = stop_stamp - start_stamp
console.error "[#{stop_stamp.format("HH:mm:ss")}] Finished after #{moment(duration).format("mm:ss")}"

correct = []

for i in [0..refs.length-1]
  pointedPoint = refs[i]
  pointedBackPoint = rev_refs[pointedPoint.id]
  
  if pointedBackPoint.id == i
    #console.log i, pointedPoint.id
    correct.push pair = 
      a:
        x:
          a.points[i].x
        y:
          a.points[i].y
      b:
        x:
          b.points[pointedPoint.id].x
        y:
          b.points[pointedPoint.id].y

  #else
  #  console.log "#{i} != #{pointedBackPoint.id}"

console.log JSON.stringify {dimensions: {a: a.dimensions, b: b.dimensions}, pairs: correct}
