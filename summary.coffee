fs = require 'fs'

console.log "Pairs"

for name in ["a", "b", "c", "d", "e", "f", "g", "h"]
  parsed = JSON.parse(fs.readFileSync("tmp/#{name}_pairs.json", 'utf-8'))
  console.log "#{name} #{parsed.pairs.length}"
  
console.log ""
console.log "Closest"
  
for name in ["a", "b", "c", "d", "e", "f", "g", "h"]
  parsed = JSON.parse(fs.readFileSync("tmp/#{name}_closest.json", 'utf-8'))
  console.log "#{name} #{parsed.pairs.length}"
  
console.log ""
console.log "Affine"
  
for name in ["a", "b", "c", "d", "e", "f", "g", "h"]
  parsed = JSON.parse(fs.readFileSync("tmp/#{name}_affine.json", 'utf-8'))
  console.log "#{name} #{parsed.pairs.length}"
